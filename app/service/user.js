const { user } = require("../models");

module.exports = {
    getAllUser(a) {
        const users = user.findAll(a)
        return users 
    },
    createUser(email, password, address){
        const users = user.create({
            email, password, address
        });
        return users;
    },

    updateUser(user, userUpdate){
        return user.update(userUpdate)
    },
    deleteUser(user){
        return user.destroy()
    },
    findKey(findIdUser){
        return user.findByPk(findIdUser)
    }
}
