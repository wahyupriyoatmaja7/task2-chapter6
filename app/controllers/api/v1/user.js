const { user, shirt } = require("../../../models");
const userService = require("../../../service/user");
const post = require("./post");
// const shirt = require("./shirt");

module.exports = {
  list(req, res) {
      userService.getAllUser({
        include :{
          model : post,
          attributes: [
            "user_id", "judul", "penulis", "price"
          ]
        }
      })
      .then((user) => {
        res.status(200).json({
          status: "OK",
          data: {
            user,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
      

  create(req, res) {
    const { email, password, address} = req.body;
    userService.createUser( email, password, address )
      .then((user) => {
        res.status(201).json({
          status: "OK",
          data: user,
        });
      })
      .catch((err) => {
        res.status(401).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    const user = req.user;
    userService.updateUser( user, req.body )
      .then(() => {
        res.status(200).json({
          status: "OK",
          data: user,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const user = req.user;

    res.status(200).json({
      status: "OK",
      data: user,
    });
  },

  destroy(req, res) {
    userService
    .deleteUser(req.user)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setUser(req, res, next) {
    userService.findKey(req.params.id)
      .then((user) => {
        if (!user) {
          res.status(404).json({
            status: "FAIL",
            message: "Post not found!",
          });

          return;
        }

        req.user = user;
        next()
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Post not found!",
        });
      });
  },
};
