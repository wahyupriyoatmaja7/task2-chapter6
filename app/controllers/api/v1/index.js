/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

// const post = require("./post");
const user = require("./user");
const post = require("./post");

module.exports = {
  user,
  post
};
