'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('users',[{
     email : 'wahyupriyo1707@gmail.com',
     password : '12345',
     address : "Demak",
     createdAt: new Date(),
     updatedAt: new Date()

   }]);

   await queryInterface.bulkInsert('post',[{
    user_id: '1',
    judul : 'Hujan',
    penulis : "imam",
    price : 50000,
    createdAt: new Date(),
    updatedAt: new Date()

  }]);
  
  },
  
  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('users', null, {})
     await queryInterface.bulkDelete('post', null, {})
  }
};
